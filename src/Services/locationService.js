export default {
    getAllLocations(app_id,app_secret){
        let brand_id = {
            "brand_id" : "25",
        };
        const requestOptions = {
            method: "POST",
            body: JSON.stringify(brand_id),
            headers: {
                "Content-Type": "application/json",
                "app-id" : app_id,
                "app-secret" : app_secret
            }
        };
        return fetch("https://multivendor.dev.api.unoapp.io/api/app/locations/all", requestOptions)
            .then(response => {
                return response.json();
            })
    },
    getMenuByLocationId(app_id,app_secret){
        let id = 113;
        const requestOptions = {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "app-id" : app_id,
                "app-secret" : app_secret
            }
        };
        return fetch("https://multivendor.dev.api.unoapp.io/api/app/menus/full/"+ id, requestOptions)
            .then(response => {
                return response.json();
            })
    }
}