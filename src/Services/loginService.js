export default {
    login(user_name,password){
        let loginCedentials = {
            "user_name" : user_name,
            "password" : password
        };
        const requestOptions = {
            method: "POST",
            body: JSON.stringify(loginCedentials),
            headers: {
                "Content-Type": "application/json",
            }
        };
        return fetch("https://multivendor.dev.api.unoapp.io/api/application/admin/login", requestOptions)
            .then(response => {
                return response.json();
            })
    },
    logOut(app_id,app_secret,token){
        const requestOptions = {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "app-id" : app_id,
                "app-secret" : app_secret,
                "auth-token" : token
            }
        };
        return fetch("https://multivendor.dev.api.unoapp.io/api/application/admin/logout", requestOptions)
            .then(response => {
                return response.json();
            })
    }
}