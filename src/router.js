import Vue from "vue";
import Router from "vue-router";
import login from "./components/loginPage/login.vue";
import menu from "./components/menuPage/menu.vue";
import locations from "./components/locationList/locations.vue"
Vue.use(Router);
export default new Router({
    mode: 'history',
    routes: [
        {
            path: "/",
            name: "login",
            component: login
        },
        {
            path: "*",
            component: login
        },
        {
            path: "/menu",
            name: "menu",
            component: menu
        },
        {
            path: "/locations",
            name: "locations",
            component: locations
        }
    ]
})